<?php
class Error extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

        function index()
        {
            $this->view->msg = "Page is not exists";
            $this->view->render('error/index');
        }
}